pushd .
cd ..\raw_data
curl -o element_scaling.csv -L "https://docs.google.com/spreadsheets/d/1ybiI1WgyRs67kGklUXeroi58KDab2KsU_hKutBORiW0/export?gid=1749106382&format=csv"
curl -o weapons.csv -L "https://docs.google.com/spreadsheets/d/1ybiI1WgyRs67kGklUXeroi58KDab2KsU_hKutBORiW0/export?gid=1618004544&format=csv"
curl -o reinforce_values.csv -L "https://docs.google.com/spreadsheets/d/1ybiI1WgyRs67kGklUXeroi58KDab2KsU_hKutBORiW0/export?gid=399958675&format=csv"
curl -o passives.csv -L "https://docs.google.com/spreadsheets/d/1ybiI1WgyRs67kGklUXeroi58KDab2KsU_hKutBORiW0/export?gid=1571528518&format=csv"
curl -o correction_values.csv -L "https://docs.google.com/spreadsheets/d/1ybiI1WgyRs67kGklUXeroi58KDab2KsU_hKutBORiW0/export?gid=2004822371&format=csv"

curl -o weapon_id_motion_names.csv -L "https://docs.google.com/spreadsheets/d/1j4bpTbsnp5Xsgw9TP2xv6d8R4qk0ErpE9r_5LGIDraU/export?gid=809407445&format=csv"
curl -o motion_values.csv -L "https://docs.google.com/spreadsheets/d/1j4bpTbsnp5Xsgw9TP2xv6d8R4qk0ErpE9r_5LGIDraU/export?gid=0&format=csv"
curl -o swing_values.csv -L "https://docs.google.com/spreadsheets/d/1j4bpTbsnp5Xsgw9TP2xv6d8R4qk0ErpE9r_5LGIDraU/export?gid=506566839&format=csv"
curl -o stamina_values.csv -L "https://docs.google.com/spreadsheets/d/1j4bpTbsnp5Xsgw9TP2xv6d8R4qk0ErpE9r_5LGIDraU/export?gid=1587575206&format=csv"

curl -o bosses.csv -L "https://docs.google.com/spreadsheets/d/1aujq95UfL_oUs3voPt3nGqM1hLhaVJOj6JKB6Np3FD8/export?gid=1466577964&format=csv"
curl -o bosses_detail.csv -L "https://docs.google.com/spreadsheets/d/1aujq95UfL_oUs3voPt3nGqM1hLhaVJOj6JKB6Np3FD8/export?gid=210015545&format=csv"
curl -o stat_data.csv -L "https://docs.google.com/spreadsheets/d/1aujq95UfL_oUs3voPt3nGqM1hLhaVJOj6JKB6Np3FD8/export?gid=1315305173&format=csv"
curl -o location_scaling.csv -L "https://docs.google.com/spreadsheets/d/1aujq95UfL_oUs3voPt3nGqM1hLhaVJOj6JKB6Np3FD8/export?gid=466524610&format=csv"
curl -o difficulty_scaling.csv -L "https://docs.google.com/spreadsheets/d/1aujq95UfL_oUs3voPt3nGqM1hLhaVJOj6JKB6Np3FD8/export?gid=413036149&format=csv"
curl -o resist_corrections.csv -L "https://docs.google.com/spreadsheets/d/1aujq95UfL_oUs3voPt3nGqM1hLhaVJOj6JKB6Np3FD8/export?gid=2116417499&format=csv"

curl -o armor.csv -L "https://docs.google.com/spreadsheets/d/1x6LvzrqA9LWXPbzPZBDG8aL4N3Xc_ZxtEFMWpUxQj5c/export?gid=1685478111&format=tsv"
groovy -e "def file=new File('armor.csv');file.text=file.text.replace(',',' ').replace('\t',',')"
popd