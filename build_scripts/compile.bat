pushd .
cd ..\src
call java -jar ..\build_scripts\compiler.jar --js_output_file temp.js *.js
call javascript-obfuscator temp.js -o ../script.js --self-defending true --log true
del temp.js
popd