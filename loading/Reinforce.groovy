reinforce_values = csvToObject('reinforce_values').collectEntries{ key, entry ->
    [key,
        [
            physical: entry['Physical Attack'] as double,
            magic: entry['Magic Attack'] as double,
            fire: entry['Fire Attack'] as double,
            lightning: entry['Lightning Attack'] as double,
            holy: entry['Holy Attack'] as double,
            str: entry['Str Scaling'] as double,
            dex: entry['Dex Scaling'] as double,
            int: entry['Int Scaling'] as double,
            fai: entry['Fai Scaling'] as double,
            arc: entry['Arc Scaling'] as double,
            passive_offset_1: entry['Effect 1 Offset'] as int,
            passive_offset_2: entry['Effect 2 Offset'] as int,
        ]
    ]
}

build_json 'reinforce_values'