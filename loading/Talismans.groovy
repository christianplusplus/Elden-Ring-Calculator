import groovy.json.*

def talismans = csvToList('talismans').collect{
    it.weight = it.weight as double
    it.effects = parse_effects(it.effects)
    it
}
talismans.sort{ it.name }
talismans.push(['name':'None'])
new File(targetDirectory, 'talismans.json').write(JsonOutput.toJson(talismans))
println "Updated \"talismans.json\"."