sourceDirectory = '../raw_data'
targetDirectory = '../data'

evaluate(new File('Util.groovy'))
evaluate(new File('Targets.groovy'))
evaluate(new File('LocationScaling.groovy'))
evaluate(new File('DifficultyScaling.groovy'))
evaluate(new File('MotionValues.groovy'))
evaluate(new File('Passives.groovy'))
evaluate(new File('Reinforce.groovy'))
evaluate(new File('Weapons.groovy'))
evaluate(new File('CorrectionValues.groovy'))
evaluate(new File('ResistCorrections.groovy'))
evaluate(new File('Armor.groovy'))
evaluate(new File('Talismans.groovy'))

[
    'weapon_buffs',
    'shield_buffs',
    'body_buffs',
    'aura_buffs',
    'stackable_buffs',
].each{ load_modifiers it }