import groovy.json.*

ranged_weapon_types = ['Light Bow', 'Bow', 'Greatbow', 'Crossbow', 'Ballista']

damage_types = ['physical', 'magic', 'fire', 'lightning', 'holy']

sources = ['str', 'dex', 'int', 'fai', 'arc']

passive_map = [
    Blood: 'bleed',
    Rot: 'rot',
    Death: 'death',
    Sleep: 'sleep',
    Poison: 'poison',
    Frost: 'frost',
    Madness: 'madness',
]

weapon_type_map = [
    '1': 'Dagger',
    '3': 'Straight Sword',
    '9': 'Curved Sword',
    '5': 'Greatsword',
    '7': 'Colossal Sword',
    '15': 'Thrusting Sword',
    '16': 'Heavy Thrusting Sword',
    '11': 'Curved Greatsword',
    '13': 'Katana',
    '14': 'Twinblade',
    '21': 'Hammer',
    '23': 'Great Hammer',
    '24': 'Flail',
    '17': 'Axe',
    '19': 'Greataxe',
    '25': 'Spear',
    '28': 'Greate Spear',
    '29': 'Halberd',
    '31': 'Reaper',
    '33': 'Unarmed',
    '39': 'Whip',
    '35': 'Fist',
    '37': 'Claw',
    '41': 'Colossal Weapon',
    '87': 'Torch',
    '65': 'Small Shield',
    '67': 'Medium Shield',
    '69': 'Greateshield',
    '57': 'Glintstone Staff',
    '61': 'Sacred Seal',
    '50': 'Light Bow',
    '51': 'Bow',
    '53': 'Greatbow',
    '55': 'Crossbow',
    '56': 'Ballista',
]

weapon_affinity_map = [
    '0': 'Standard',
    '1900': 'Standard',
    '3100': 'Standard',
    '8000': 'Standard',
    '8100': 'Standard',
    '8200': 'Standard',
    
    '100': 'Heavy',
    '6000': 'Heavy',
    
    '200': 'Keen',
    '5000': 'Keen',
    
    '300': 'Quality',
    
    '400': 'Fire',
    
    '500': 'Flame Art',
    
    '600': 'Lightning',
    
    '700': 'Sacred',
    
    '800': 'Magic',
    
    '900': 'Cold',
    
    '1000': 'Poison',
    
    '1100': 'Blood',
    
    '1200': 'Occult',
    
    '2200': 'Somber',
    '2400': 'Somber',
    '3200': 'Somber',
    '3300': 'Somber',
    '8300': 'Somber',
    '8500': 'Somber',
    
    '3000': 'Unupgradable',//'Somber',//
]

csvSplitterExpression = /,(?=(?:[^"]*"[^"]*")*[^"]*$)/

csvOuterQuoteExpression = /^"|"$/

getLineList = { String line -> line.split(csvSplitterExpression).collect{it.replaceAll(csvOuterQuoteExpression, '')} }

List.metaClass.collectWithIndex = { yield ->
    def collected = []
    delegate.eachWithIndex { listItem, index ->
        collected << yield(listItem, index)
    }
    
    return collected 
}

getRepeatingColumnList = { String line ->
    def i = 0
    line.split(csvSplitterExpression).collect{
        it.replaceAll(csvOuterQuoteExpression, '') + "#${i++}"
    }
}

csvToObject = { String fileName ->
    def lines = new File(sourceDirectory, fileName + ".csv").readLines()
    def columns = getLineList(lines[0])
    def object = lines.tail().collect{
        getLineList(it)
    }.collectEntries{
        [it[0], [columns, it].transpose().collectEntries()]
    }
}

motionCsvToObject = { String fileName ->
    def lines = new File(sourceDirectory, fileName + ".csv").readLines()
    def columns = getLineList(lines[0])
    def object = lines.tail().collect{
        getLineList(it)
    }.findAll{
        it[0] && it[0].length() > 0
    }.collectEntries{
        [it[1], [columns, it].transpose().collectEntries()]
    }
}

csvToList = { String fileName ->
    def lines = new File(sourceDirectory, fileName + ".csv").readLines()
    def columns = getLineList(lines[0])
    def object = lines.tail().collect{
        getLineList(it)
    }.collect{
        [columns, it].transpose().collectEntries()
    }
}

csvBossesToList = { String fileName ->
    def lines = new File(sourceDirectory, fileName + ".csv").readLines()
    def columns = getRepeatingColumnList(lines[0])
    def object = lines.tail().collect{
        getLineList(it)
    }.collect{
        [columns, it].transpose().collectEntries()
    }
}

last_3_chars = {String string -> string.size() < 3 ? string : string.substring(string.size() - 3)}

string_number_to_int = { Math.round(java.text.NumberFormat.instance.parse(it)) as int }

build_json = { String it ->
    new File(targetDirectory, "${it}.json").write(JsonOutput.toJson(binding.getVariable it))
    println "Updated \"${it}.json\"."
}

parse_effects = { def effects, def requirements = null, def enchantment = null ->
    if(effects) {
        effects = effects.split(/\+/).collect{ effect ->
            effect = effect.split(/\s+/, 4) as List
            def modifier = [:]
            switch(effect[2]) {
                case 'mapped':
                    modifier.correction_id = effect[3]
                    break;
                case 'str':
                case 'dex':
                case 'int':
                case 'fai':
                case 'arc':
                    effect[3] = effect[3].split(/\s+/)
                    modifier.value = effect[3][0] as double
                    modifier.correction_id = effect[3][1]
                    break;
                default:
                    modifier.value = effect[3] as double
            }
            effect[3] = modifier
            effect
        }
        
        if(requirements) {
            requirements = requirements.split(/\s+/).collectEntries{ req ->
                req = req.split(':')
                [req[0], req[1] as int]
            }
            effects.each {
                it[3].requirements = requirements
            }
        }
        
        if(enchantment) {
            enchantment = enchantment.equalsIgnoreCase('true')
            effects.each {
                it[3].enchantment = enchantment
            }
        }
    }
    else {
        effects = null
    }
    effects
}

load_modifiers = { table_name ->
    def list = csvToList(table_name).collect{
        it.effects = parse_effects(it.effects, it.requirements, it.enchantment)
        it.remove('requirements')
        it.remove('enchantment')
        it
    }
    list.sort{ it.name }
    list.push(['name':'None'])
    new File(targetDirectory, table_name + '.json').write(JsonOutput.toJson(list))
    println "Updated \"${table_name}.json\"."
}

load_gear_modifier = { table_name ->
    def list = csvToList(table_name).collectEntries{
        it.effects = parse_effects(it.effects, it.requirements, it.enchantment)
        it.remove('requirements')
        it.remove('enchantment')
        [it.name, it]
    }
}