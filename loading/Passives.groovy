passives = csvToObject('passives').collectEntries{ key, entry ->
    [key as int,
        [
            value: entry['Value'] as int,
            type: passive_map[entry['Type']],
        ]
    ]
}

//Fixes for "deadly" passives; Just Poison and Bleed for now.
(6510..6515).each{passives[it].type = 'deadly_poison'}
(106050..106075).each{passives[it].type = 'super_deadly_poison'}
(6410..6415).each{passives[it].type = 'deadly_bleed'}
(105050..105075).each{passives[it].type = 'deadly_bleed'}
(105150..105175).each{passives[it].type = 'deadly_bleed'}
(105250..105275).each{passives[it].type = 'deadly_bleed'}

build_json 'passives'