def bosses_detail = csvToObject('bosses_detail')

def stat_data = csvToObject('stat_data')

bosses = csvBossesToList('bosses').collectMany{
    def id1, id2, id3, id4, multiplier
    try{
        def list = []
        id1 = it['NPC ID 1#0']
        id2 = it['NPC ID 2#1']
        id3 = it['NPC ID 3#2']
        id4 = it['NPC ID (Phase 2)#3']
        
        list << [
            name: it['Name 1#11'],
            health: string_number_to_int(it['HP#12']),
            location_id: it['SpEffect ID#5'],
            ngp_id: it['SpEffect ID (NG+)#6'],
            standard: (100 - (it['Standard Negation#14'] as int)) / 100,
            slash: (100 - (it['Slash Negation#15'] as int)) / 100,
            strike: (100 - (it['Strike Negation#16'] as int)) / 100,
            pierce: (100 - (it['Pierce Negation#17'] as int)) / 100,
            magic: (100 - (it['Magic Negation#18'] as int)) / 100,
            fire: (100 - (it['Fire Negation#19'] as int)) / 100,
            lightning: (100 - (it['Lightning Negation#20'] as int)) / 100,
            holy: (100 - (it['Holy Negation#21'] as int)) / 100,
            poison: it['Poison#22'] == 'IMMUNE' ? -1 : string_number_to_int(it['Poison#22']),
            rot: it['Rot#23'] == 'IMMUNE' ? -1 : string_number_to_int(it['Rot#23']),
            bleed: it['Blood#24'] == 'IMMUNE' ? -1 : string_number_to_int(it['Blood#24']),
            frost: it['Frost#25'] == 'IMMUNE' ? -1 : string_number_to_int(it['Frost#25']),
            sleep: it['Sleep#26'] == 'IMMUNE' ? -1 : string_number_to_int(it['Sleep#26']),
            madness: it['Madness#27'] == 'IMMUNE' ? -1 : string_number_to_int(it['Madness#27']),
            death: it['Death Blight#28'] == 'IMMUNE' ? -1 : string_number_to_int(it['Death Blight#28']),
            resist_correction_id:[
                poison: last_3_chars(bosses_detail[id1]['resistCorrectId_poison']) as int,
                rot: last_3_chars(bosses_detail[id1]['resistCorrectId_disease']) as int,
                bleed: last_3_chars(bosses_detail[id1]['resistCorrectId_blood']) as int,
                frost: last_3_chars(bosses_detail[id1]['resistCorrectId_freeze']) as int,
                sleep: last_3_chars(bosses_detail[id1]['resistCorrectId_sleep']) as int,
                madness: last_3_chars(bosses_detail[id1]['resistCorrectId_madness']) as int,
                death: last_3_chars(bosses_detail[id1]['resistCorrectId_curse']) as int,
            ],
            type: bosses_detail[id1].isWeakA == '1' ? 'void' :
                bosses_detail[id1].isWeakB == '1' ? 'undead' :
                bosses_detail[id1].isWeakC == '1' ? 'ancient_dragon' :
                bosses_detail[id1].isWeakD == '1' ? 'dragon' :
                'normal',
        ]
        
        multiplier = stat_data[id1]['Blood Damage Multiplier'] as double
        if(multiplier != 1.0)
            list.last().blood_multiplier =  multiplier
        multiplier = stat_data[id1]['Frost Damage Multiplier'] as double
        if(multiplier != 1.0)
            list.last().frost_multiplier =  multiplier
            
        
        if(id2 != '-') {
            list << [
                name: it['Name 2#31'],
                health: string_number_to_int(it['HP#32']),
                location_id: it['SpEffect ID#5'],
                ngp_id: it['SpEffect ID (NG+)#6'],
                standard: (100 - (it['Standard Negation#34'] as int)) / 100,
                slash: (100 - (it['Slash Negation#35'] as int)) / 100,
                strike: (100 - (it['Strike Negation#36'] as int)) / 100,
                pierce: (100 - (it['Pierce Negation#37'] as int)) / 100,
                magic: (100 - (it['Magic Negation#38'] as int)) / 100,
                fire: (100 - (it['Fire Negation#39'] as int)) / 100,
                lightning: (100 - (it['Lightning Negation#40'] as int)) / 100,
                holy: (100 - (it['Holy Negation#41'] as int)) / 100,
                poison: it['Poison#42'] == 'IMMUNE' ? -1 : string_number_to_int(it['Poison#42']),
                rot: it['Rot#43'] == 'IMMUNE' ? -1 : string_number_to_int(it['Rot#43']),
                bleed: it['Blood#44'] == 'IMMUNE' ? -1 : string_number_to_int(it['Blood#44']),
                frost: it['Frost#45'] == 'IMMUNE' ? -1 : string_number_to_int(it['Frost#45']),
                sleep: it['Sleep#46'] == 'IMMUNE' ? -1 : string_number_to_int(it['Sleep#46']),
                madness: it['Madness#47'] == 'IMMUNE' ? -1 : string_number_to_int(it['Madness#47']),
                death: it['Death Blight#48'] == 'IMMUNE' ? -1 : string_number_to_int(it['Death Blight#48']),
                resist_correction_id:[
                    poison: last_3_chars(bosses_detail[id2]['resistCorrectId_poison']) as int,
                    rot: last_3_chars(bosses_detail[id2]['resistCorrectId_disease']) as int,
                    bleed: last_3_chars(bosses_detail[id2]['resistCorrectId_blood']) as int,
                    frost: last_3_chars(bosses_detail[id2]['resistCorrectId_freeze']) as int,
                    sleep: last_3_chars(bosses_detail[id2]['resistCorrectId_sleep']) as int,
                    madness: last_3_chars(bosses_detail[id2]['resistCorrectId_madness']) as int,
                    death: last_3_chars(bosses_detail[id2]['resistCorrectId_curse']) as int,
                ],
                type: bosses_detail[id2].isWeakA == '1' ? 'void' :
                    bosses_detail[id2].isWeakB == '1' ? 'undead' :
                    bosses_detail[id2].isWeakC == '1' ? 'ancient_dragon' :
                    bosses_detail[id2].isWeakD == '1' ? 'dragon' :
                    'normal',
            ]
            
            multiplier = stat_data[id2]['Blood Damage Multiplier'] as double
            if(multiplier != 1.0)
                list.last().blood_multiplier =  multiplier
            multiplier = stat_data[id2]['Frost Damage Multiplier'] as double
            if(multiplier != 1.0)
                list.last().frost_multiplier =  multiplier
        }
        
        if(id3 != '-') {
            list << [
                name: it['Name 3#51'],
                health: string_number_to_int(it['HP#52']),
                location_id: it['SpEffect ID#5'],
                ngp_id: it['SpEffect ID (NG+)#6'],
                standard: (100 - (it['Standard Negation#54'] as int)) / 100,
                slash: (100 - (it['Slash Negation#55'] as int)) / 100,
                strike: (100 - (it['Strike Negation#56'] as int)) / 100,
                pierce: (100 - (it['Pierce Negation#57'] as int)) / 100,
                magic: (100 - (it['Magic Negation#58'] as int)) / 100,
                fire: (100 - (it['Fire Negation#59'] as int)) / 100,
                lightning: (100 - (it['Lightning Negation#60'] as int)) / 100,
                holy: (100 - (it['Holy Negation#61'] as int)) / 100,
                poison: it['Poison#62'] == 'IMMUNE' ? -1 : string_number_to_int(it['Poison#62']),
                rot: it['Rot#63'] == 'IMMUNE' ? -1 : string_number_to_int(it['Rot#63']),
                bleed: it['Blood#64'] == 'IMMUNE' ? -1 : string_number_to_int(it['Blood#64']),
                frost: it['Frost#65'] == 'IMMUNE' ? -1 : string_number_to_int(it['Frost#65']),
                sleep: it['Sleep#66'] == 'IMMUNE' ? -1 : string_number_to_int(it['Sleep#66']),
                madness: it['Madness#67'] == 'IMMUNE' ? -1 : string_number_to_int(it['Madness#67']),
                death: it['Death Blight#68'] == 'IMMUNE' ? -1 : string_number_to_int(it['Death Blight#68']),
                resist_correction_id:[
                    poison: last_3_chars(bosses_detail[id3]['resistCorrectId_poison']) as int,
                    rot: last_3_chars(bosses_detail[id3]['resistCorrectId_disease']) as int,
                    bleed: last_3_chars(bosses_detail[id3]['resistCorrectId_blood']) as int,
                    frost: last_3_chars(bosses_detail[id3]['resistCorrectId_freeze']) as int,
                    sleep: last_3_chars(bosses_detail[id3]['resistCorrectId_sleep']) as int,
                    madness: last_3_chars(bosses_detail[id3]['resistCorrectId_madness']) as int,
                    death: last_3_chars(bosses_detail[id3]['resistCorrectId_curse']) as int,
                ],
                type: bosses_detail[id3].isWeakA == '1' ? 'void' :
                    bosses_detail[id3].isWeakB == '1' ? 'undead' :
                    bosses_detail[id3].isWeakC == '1' ? 'ancient_dragon' :
                    bosses_detail[id3].isWeakD == '1' ? 'dragon' :
                    'normal',
            ]
            
            multiplier = stat_data[id3]['Blood Damage Multiplier'] as double
            if(multiplier != 1.0)
                list.last().blood_multiplier =  multiplier
            multiplier = stat_data[id3]['Frost Damage Multiplier'] as double
            if(multiplier != 1.0)
                list.last().frost_multiplier =  multiplier
        }
        
        if(id4 != '-') {
            list << [
                name: it['Actual Name (Phase 2)#70'] + ' (2nd Phase)',
                health: string_number_to_int(it['HP#71']),
                location_id: it['SpEffect ID#5'],
                ngp_id: it['SpEffect ID (NG+)#6'],
                standard: (100 - (it['Standard Negation#73'] as int)) / 100,
                slash: (100 - (it['Slash Negation#74'] as int)) / 100,
                strike: (100 - (it['Strike Negation#75'] as int)) / 100,
                pierce: (100 - (it['Pierce Negation#76'] as int)) / 100,
                magic: (100 - (it['Magic Negation#77'] as int)) / 100,
                fire: (100 - (it['Fire Negation#78'] as int)) / 100,
                lightning: (100 - (it['Lightning Negation#79'] as int)) / 100,
                holy: (100 - (it['Holy Negation#80'] as int)) / 100,
                poison: it['Poison#81'] == 'IMMUNE' ? -1 : string_number_to_int(it['Poison#81']),
                rot: it['Rot#82'] == 'IMMUNE' ? -1 : string_number_to_int(it['Rot#82']),
                bleed: it['Blood#83'] == 'IMMUNE' ? -1 : string_number_to_int(it['Blood#83']),
                frost: it['Frost#84'] == 'IMMUNE' ? -1 : string_number_to_int(it['Frost#84']),
                sleep: it['Sleep#85'] == 'IMMUNE' ? -1 : string_number_to_int(it['Sleep#85']),
                madness: it['Madness#86'] == 'IMMUNE' ? -1 : string_number_to_int(it['Madness#86']),
                death: it['Death Blight#87'] == 'IMMUNE' ? -1 : string_number_to_int(it['Death Blight#87']),
                resist_correction_id:[
                    poison: last_3_chars(bosses_detail[id4]['resistCorrectId_poison']) as int,
                    rot: last_3_chars(bosses_detail[id4]['resistCorrectId_disease']) as int,
                    bleed: last_3_chars(bosses_detail[id4]['resistCorrectId_blood']) as int,
                    frost: last_3_chars(bosses_detail[id4]['resistCorrectId_freeze']) as int,
                    sleep: last_3_chars(bosses_detail[id4]['resistCorrectId_sleep']) as int,
                    madness: last_3_chars(bosses_detail[id4]['resistCorrectId_madness']) as int,
                    death: last_3_chars(bosses_detail[id4]['resistCorrectId_curse']) as int,
                ],
                type: bosses_detail[id4].isWeakA == '1' ? 'void' :
                    bosses_detail[id4].isWeakB == '1' ? 'undead' :
                    bosses_detail[id4].isWeakC == '1' ? 'ancient_dragon' :
                    bosses_detail[id4].isWeakD == '1' ? 'dragon' :
                    'normal',
            ]
            
            multiplier = stat_data[id4]['Blood Damage Multiplier'] as double
            if(multiplier != 1.0)
                list.last().blood_multiplier =  multiplier
            multiplier = stat_data[id4]['Frost Damage Multiplier'] as double
            if(multiplier != 1.0)
                list.last().frost_multiplier =  multiplier
        }
        
        list
    }catch(e){
        println it
        throw e
    }
}

bosses << [['name', 'health', 'location_id', 'ngp_id', 'standard','slash','strike','pierce','magic','fire','lightning','holy','poison','rot','bleed','frost','sleep','madness','death','resist_correction_id','type','blood_multiplier','frost_multiplier'],['Target Dummy', 8000,'0','0',1,1,1,1,1,1,1,1,400,400,400,400,400,400,400,['poison':0,'rot':0,'bleed':0,'frost':0,'sleep':0,'death':0,'madness':0],'normal',0.7,0.7]].transpose().collectEntries()

bosses.sort{ a, b -> a.name <=> b.name }

build_json 'bosses'