location_scaling = csvToObject('location_scaling').collectEntries{ key, entry ->
    try{
        [key, 
            [
                health: entry['HP'] as double,
                defense: entry['Defense'] as double,
                poison: entry['Poison'] as double,
                rot: entry['Rot'] as double,
                bleed: entry['Blood'] as double,
                frost: entry['Frost'] as double,
                sleep: entry['Sleep'] as double,
                madness: entry['Madness'] as double,
                death: entry['Death Blight'] as double,
            ]
        ]
    }catch(e){
        println key
        println entry
        throw e
    }
}

build_json 'location_scaling'