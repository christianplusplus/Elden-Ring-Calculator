helms = []
cuirasses = []
gauntlets = []
greaves = []


def helm_buffs = load_gear_modifier('helm_buffs')
def cuirass_buffs = load_gear_modifier('cuirass_buffs')
def gauntlet_buffs = load_gear_modifier('gauntlet_buffs')
def greave_buffs = load_gear_modifier('greave_buffs')

def slot_map = [
    'Head': helms,
    'Body': cuirasses,
    'Arm': gauntlets,
    'Leg': greaves,
]

def modifiers_map = [
    'Head': helm_buffs,
    'Body': cuirass_buffs,
    'Arm': gauntlet_buffs,
    'Leg': greave_buffs,
]

csvToList('armor').findAll{it['Obtainable'] == 'Yes'}.each{
    try{
        def item = [
            id: it['ID'],
            name: it['Name'],
            weight: it['Weight'] as double,
            negation: [
                standard: it['Damage Negation (Physical)'] as double,
                slash: it['Damage Negation (VS Strike)'] as double,
                strike: it['Damage Negation (VS Slash)'] as double,
                pierce: it['Damage Negation (VS Pierce)'] as double,
                magic: it['Damage Negation (Magic)'] as double,
                fire: it['Damage Negation (Fire)'] as double,
                lightning: it['Damage Negation (Lightning)'] as double,
                holy: it['Damage Negation (Holy)'] as double,
            ],
            immunity: it['Immunity (Poison and Rot)'] as int,
            robustness: it['Robustness (Blood)'] as int,
            focus: it['Focus (Madness)'] as int,
            vitality: it['Vitality (Death Blight)'] as int,
            poise: it['Poise'] as int,
            effects: null,
        ]
        
        if(modifiers_map[it['Equip Slot']].containsKey(item.name)) {
            item.effects = modifiers_map[it['Equip Slot']][item.name].effects
            if(modifiers_map[it['Equip Slot']][item.name].containsKey('addendum'))
                item.addendum = modifiers_map[it['Equip Slot']][item.name].addendum
            modifiers_map[it['Equip Slot']].remove(item.name)
        }
        
        slot_map[it['Equip Slot']] << item
        
    }catch(e){
        println it
        throw e
    }
}

slot_map.each{ it.value.sort{ it.name }; it.value.push(['name':'None']) }

def bad_modifier_list = modifiers_map.values().find{!it.isEmpty()}
if(bad_modifier_list != null)
    throw new Exception('These modifiers did not get applied to armor list.' + bad_modifier_list)

build_json 'helms'
build_json 'cuirasses'
build_json 'gauntlets'
build_json 'greaves'