import groovy.json.*

def element_scaling = csvToObject('element_scaling')

//weapon data to list
def weapon_id_motion_names = csvToList('weapon_id_motion_names').collectEntries{[it['ID'],it['Weapon']]}

def raw_weapons = csvToObject('weapons')

weapons = csvToList('weapons').findAll{it['my category'] == 'unarmed' || (it['sortId'] != '9999999' && it['my category'] != 'arrow' && it['my category'] != 'unused')}.collect{
    try{
        def weapon_type_id = it['wepType']
        if(!weapon_type_map.containsKey(weapon_type_id))
            throw new Exception("Unexpected weapon moveset ID \"$weapon_type_id\".")
            
        def reinforce_id = it['reinforceTypeId']
        if(!weapon_affinity_map.containsKey(reinforce_id))
            throw new Exception("Unexpected weapon reinforce ID \"$reinforce_id\".")
            
        def weapon = [
            id: it['ID'],
            name: it['Name'],
            base_weapon_name: it['originEquipWep'] == '-1' ? it['Name'] : raw_weapons[it['originEquipWep']]['Name'],
            motion_name: it['originEquipWep'] == '-1' ? 'Katar' : weapon_id_motion_names[it['originEquipWep']],
            //motion_category: it['wepmotionCategory'],
            reinforce_id: reinforce_id as int,
            element_scaling: [:],
            weapon_type: weapon_type_map[weapon_type_id],
            affinity: weapon_affinity_map[reinforce_id],
            weight: it['weight'] as double,
            max_upgrade_level: it['originEquipWep1'] == '-1' ? 0 : it['originEquipWep11'] == '-1' ? 10 : 25,
            crit: (it['throwAtkRate'] as int + 100) / 100.0,
            physical_damage_types: [],
            physical_attack_power: it['attackBasePhysics'] as int,
            magic_attack_power: it['attackBaseMagic'] as int,
            fire_attack_power: it['attackBaseFire'] as int,
            lightning_attack_power: it['attackBaseThunder'] as int,
            holy_attack_power: it['attackBaseDark'] as int,
            physical_correction_id: it['correctType_Physics'] as int,
            magic_correction_id: it['correctType_Magic'] as int,
            fire_correction_id: it['correctType_Fire'] as int,
            lightning_correction_id: it['correctType_Thunder'] as int,
            holy_correction_id: it['correctType_Dark'] as int,
            poison_correction_id: it['correctType_Poison'] as int,
            bleed_correction_id: it['correctType_Blood'] as int,
            sleep_correction_id: it['correctType_Sleep'] as int,
            madness_correction_id: it['correctType_Madness'] as int,
            requirements: [
                str: it['properStrength'] as int,
                dex: it['properAgility'] as int,
                int: it['properMagic'] as int,
                fai: it['properFaith'] as int,
                arc: it['properLuck'] as int,
            ],
            str_scaling : (it['correctStrength'] as double) / 100,
            dex_scaling : (it['correctAgility'] as double) / 100,
            int_scaling : (it['correctMagic'] as double) / 100,
            fai_scaling : (it['correctFaith'] as double) / 100,
            arc_scaling : (it['correctLuck'] as double) / 100,
            casts_sor: it['enableMagic'] == '1',
            casts_inc: it['enableMiracle'] == '1',
            buffable: it['isEnhance'] == '1',
            ammo: 'N/A',
            effects: [],
        ]
        
        if((weapon.casts_sor || weapon.casts_inc) && [weapon.magic_correction_id, weapon.lightning_correction_id, weapon.fire_correction_id, weapon.holy_correction_id].any{it != weapon.physical_correction_id})
            throw new Exception("Correction IDs aren't the same for catalyst $weapon.name")
        
        for(String damage_type : damage_types)
            for(String source : sources)
                if(element_scaling[it['attackElementCorrectId']]["${damage_type.capitalize()} Scaling: ${source.toUpperCase()}"] == '1')
                    weapon.element_scaling["${damage_type}_${source}"] = 1
        
        //for weapon extending.
        weapon.rot = weapon.death = weapon.madness = weapon.sleep = weapon.frost = weapon.poison = weapon.deadly_poison = weapon.super_deadly_poison = weapon.bleed = weapon.deadly_bleed = 0
        
        if(it['spEffectBehaviorId0'] != '-1')
            weapon.passive_id_1 = it['spEffectBehaviorId0'] as int
        if(it['spEffectBehaviorId1'] != '-1')
            weapon.passive_id_2 = it['spEffectBehaviorId1'] as int
        if(it['spEffectBehaviorId2'] != '-1')
            weapon.passive_id_3 = it['spEffectBehaviorId2'] as int
        
        if(it['weakA_DamageRate'] != '1')
            weapon.effects << ['target', 'void', 'percent', [value:(Math.round(it['weakA_DamageRate'] as double * 100) - 100) as int]]
        if(it['weakB_DamageRate'] != '1')
            weapon.effects << ['target', 'undead', 'percent', [value:(Math.round(it['weakB_DamageRate'] as double * 100) - 100) as int]]
        if(it['weakC_DamageRate'] != '1')
            weapon.effects << ['target', 'ancient_dragon', 'percent', [value:(Math.round(it['weakC_DamageRate'] as double * 100) - 100) as int]]
        if(it['weakD_DamageRate'] != '1')
            weapon.effects << ['target', 'dragon', 'percent', [value:(Math.round(it['weakD_DamageRate'] as double * 100) - 100) as int]]
        
        if(it['isNormalAttackType'] == '1')
            weapon['physical_damage_types'] << 'Standard'
        if(it['isBlowAttackType'] == '1')
            weapon['physical_damage_types'] << 'Strike'
        if(it['isSlashAttackType'] == '1')
            weapon['physical_damage_types'] << 'Slash'
        if(it['isThrustAttackType'] == '1')
            weapon['physical_damage_types'] << 'Pierce'
        
        weapon
    }catch(e){
        println it
        throw e
    }
}

//generate ranged weapon and ammo combinations

def ammunitions = raw_weapons.values().findAll{it['my category'] == 'arrow'}

def ammunitions_by_type = [
    'Arrow': ammunitions.findAll{it['wepType'] == '81'},
    'Greatarrow': ammunitions.findAll{it['wepType'] == '83'},
    'Bolt': ammunitions.findAll{it['wepType'] == '85'},
    'Greatbolt': ammunitions.findAll{it['wepType'] == '86'},
]

def weapon_ammo_map = [
    'Light Bow': 'Arrow',
    'Bow': 'Arrow',
    'Greatbow': 'Greatarrow',
    'Crossbow': 'Bolt',
    'Ballista': 'Greatbolt',
]

def ranged_weapons = weapons.findAll{ranged_weapon_types.contains(it['weapon_type'])}

def jsonSlurper = new JsonSlurper()
def ranged_weapons_with_ammo = ranged_weapons.collectMany{ weapon ->
    try{
        ammunitions_by_type[weapon_ammo_map[weapon['weapon_type']]].collect{ ammo ->
            
            def new_weapon = jsonSlurper.parseText(JsonOutput.toJson(weapon))
            
            new_weapon.name = "$weapon.name with ${ammo['Name']}"
            new_weapon.ammo = ammo['Name']
            new_weapon.bonus = [:]
            new_weapon.bonus.passives = []
            new_weapon.physical_damage_types << 'Pierce'
            
            if(ammo['attackBasePhysics'] != '0')
                new_weapon.bonus.physical_attack_power = ammo['attackBasePhysics'] as int
            if(ammo['attackBaseMagic'] != '0')
                new_weapon.bonus.magic_attack_power = ammo['attackBaseMagic'] as int
            if(ammo['attackBaseFire'] != '0')
                new_weapon.bonus.fire_attack_power = ammo['attackBaseFire'] as int
            if(ammo['attackBaseThunder'] != '0')
                new_weapon.bonus.lightning_attack_power = ammo['attackBaseThunder'] as int
            if(ammo['correctType_Dark'] != '0')
                new_weapon.bonus.holy_attack_power = ammo['correctType_Dark'] as int
            
            if(ammo['spEffectBehaviorId0'] != '-1')
                new_weapon.bonus.passives << (ammo['spEffectBehaviorId0'] as int)
            if(ammo['spEffectBehaviorId1'] != '-1')
                new_weapon.bonus.passives << (ammo['spEffectBehaviorId1'] as int)
            if(ammo['spEffectBehaviorId2'] != '-1')
                new_weapon.bonus.passives << (ammo['spEffectBehaviorId2'] as int)
                
            if(ammo['weakA_DamageRate'] != '1')
                new_weapon.effects << ['target', 'void', 'percent', [value:(Math.round(ammo['weakA_DamageRate'] as double * 100) - 100) as int]]
            if(ammo['weakB_DamageRate'] != '1')
                new_weapon.effects << ['target', 'undead', 'percent', [value:(Math.round(ammo['weakB_DamageRate'] as double * 100) - 100) as int]]
            if(ammo['weakC_DamageRate'] != '1')
                new_weapon.effects << ['target', 'ancient_dragon', 'percent', [value:(Math.round(ammo['weakC_DamageRate'] as double * 100) - 100) as int]]
            if(ammo['weakD_DamageRate'] != '1')
                new_weapon.effects << ['target', 'dragon', 'percent', [value:(Math.round(ammo['weakD_DamageRate'] as double * 100) - 100) as int]]
                
            if(new_weapon.name == "Lion Greatbow with Radahn's Spear")
                new_weapon.effects << ['damage', 'physical', 'percent', [value:20]]
                
            if(new_weapon.base_weapon_name == "Serpent Bow")
                new_weapon.effects << ['passive', 'poison', 'arc', [value:15, correction_id:5]]
            
            new_weapon
        }
    }catch(e){
        println weapon
        println e
        throw e
    }
}

weapons = weapons.findAll{ !ranged_weapon_types.contains(it.weapon_type) } + ranged_weapons_with_ammo

//put unarmed at end of list
weapons.add(weapons.remove(weapons.findIndexOf{weapon -> weapon.name == 'Unarmed'}))

build_json 'weapons'