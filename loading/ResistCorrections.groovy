resist_corrections = csvToObject('resist_corrections').collectEntries{ key, entry ->
    try{
        [key as int,
            [
                0,
                entry['2nd Activation'] as int,
                entry['3rd Activation'] as int,
                entry['4th Activation'] as int,
                entry['5th Activation'] as int,
                entry['6th Activation'] as int,
            ]
        ]
    }catch(e){
        println key
        println entry
        println e
        throw e
    }
}

build_json 'resist_corrections'