correction_values = csvToObject('correction_values').collectEntries{ key, entry ->
    try{
        [key,
            [
                x: [entry['Stat 0'] as int, entry['Stat 1'] as int, entry['Stat 2'] as int, entry['Stat 3'] as int, entry['Stat 4'] as int],
                y: [entry['Grow 0'] as double, entry['Grow 1'] as double, entry['Grow 2'] as double, entry['Grow 3'] as double, entry['Grow 4'] as double],
                e: [entry['Exponent 0'] as double, entry['Exponent 1'] as double, entry['Exponent 2'] as double, entry['Exponent 3'] as double, entry['Exponent 4'] as double],
            ]
        ]
    }catch(e){
        println key
        println entry
        println e
        throw e
    }
}

correction_values << csvToObject('additional_correction_values').collectEntries{ key, entry ->
    try{
        [key,
            [
                x: [entry['Stat 0'] as int, entry['Stat 1'] as int, entry['Stat 2'] as int, entry['Stat 3'] as int, entry['Stat 4'] as int],
                y: [entry['Grow 0'] as double, entry['Grow 1'] as double, entry['Grow 2'] as double, entry['Grow 3'] as double, entry['Grow 4'] as double],
                e: [entry['Exponent 0'] as double, entry['Exponent 1'] as double, entry['Exponent 2'] as double, entry['Exponent 3'] as double, entry['Exponent 4'] as double],
            ]
        ]
    }catch(e){
        println key
        println entry
        println e
        throw e
    }
}

build_json 'correction_values'