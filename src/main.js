import { createApp } from 'vue'
//import { createPinia } from 'pinia'

import App from '@/App.vue'

const components = import.meta.glob('@/components/*', { eager: true })

const app = createApp(App)

Object.entries(components).forEach(([path, definition]) => {
    const componentName = path.split('/').pop().replace(/\.\w+$/, '')
    app.component(componentName, definition.default)
  })

//app.use(createPinia())
//app.use(router)

window.main = app.mount('#app')
