self['capitalize'] = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

self['sum'] = function(a, b) {
    return a + b;
}

self['attributes_meets_requirements'] = function(attributes, requirements) {
    //requirements is allowed to be null this way.
    for(var attribute in requirements)
        if(attributes[attribute] < requirements[attribute])
            return false;
    return true;
}

function mapObject(object, func) {
    return Object.keys(object).reduce(function(result, key) {
        result[key] = func(object[key]);
        return result;
    }, {});
}

function* inclusiveRange(start, end = null, step = 1) {
    if (end === null) {
        end = start;
        start = 0;
    }
    var current = start;
    while (current <= end) {
        yield current;
        current += step;
    }
}

function* build_powerset(array, offset = 0) {
    while(offset < array.length) {
        let first = array[offset++];
        for(let subset of build_powerset(array, offset)) {
            subset.push(first);
            yield subset;
        }
    }
    yield [];
}

function key_combinations(array_of_objects) {
    return cartesian_iterator(array_of_objects.map(object => Object.entries(object)));
}

function cartesian_iterator(arrays) {
    if(!arrays.length || arrays.some(array => !array.length))
        return [];
    return cartesian_iterator_helper(...arrays);
}

function* cartesian_iterator_helper(head, ...tail) {
    const remainder = tail.length ? cartesian_iterator_helper(...tail) : [[]];
    for(let r of remainder)
        for(let h of head)
            yield [h, ...r];
}

function* binary_powerset(array) {
    for(let i = 0; i < 2 ** array.length; i++) {
        let included = [];
        let excluded = [];
        for(let j = 0; j < array.length; j++) {
            if(i & (1 << j))
                included.push(array[j]);
            else
                excluded.push(array[j]);
        }
        yield {included: included, excluded: excluded};
    }
}

function* permutateKeys(objects) {
    var iterators = mapObject(objects, l => l.values());
    var permutation = mapObject(iterators, i => i.next().value);
    yield Object.assign({}, permutation);

    var keys = Object.keys(objects);
    var index = 0;
    while(index < keys.length) {
        var key = keys[index];
        var next = iterators[key].next();
        if(next.done) {
            iterators[key] = objects[key].values();
            permutation[key] = iterators[key].next().value;
            index++;
        }
        else {
            permutation[key] = next.value;
            index = 0;
            yield Object.assign({}, permutation);
        }
    }
}

function get_attack_attribute_sum(attributes) {
    return attack_sources.map(source => attributes[source]).reduce(sum);
}