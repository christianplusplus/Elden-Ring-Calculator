function get_all_passive_hit_damage(weapon, target, modified_attributes, hit_damage, stamina_cost, modifiers, hit_modifier_value) {
    var aggregate_buildup = get_aggregate_buildup(weapon, modified_attributes, modifiers);
    
    return Object.keys(aggregate_buildup)
        .map(passive_id =>
            get_passive_hit_damage(
                aggregate_buildup[passive_id],
                passive_id,
                weapon,
                target,
                hit_damage,
                stamina_cost,
                modifiers,
                hit_modifier_value
            )
        )
        .reduce(sum);
}

function get_aggregate_buildup(weapon, modified_attributes, modifiers) {
    var effective_buildup_types = get_effective_buildup_types(weapon);
    var merged_buildup = get_merged_buildup(weapon, effective_buildup_types);
    
    var aggregate_buildup = {};
    Object.keys(passive_type_groups).forEach(passive_group_id => {
        passive_type_groups[passive_group_id].forEach(passive_id => {
            aggregate_buildup[passive_id] = effective_buildup_types[passive_group_id] == passive_id ?
                get_scaled_and_modified_passive(
                    passive_type_groups[passive_group_id]
                        .map(passive => weapon[passive])
                        .reduce(sum),
                    passive_group_id,
                    weapon,
                    modified_attributes,
                    modifiers
                ) :
                0;
        })
    });
    return aggregate_buildup;
}

function get_effective_buildup_types(weapon) {
    var effective_buildup_types = {};
    Object.keys(passive_type_groups).forEach(group =>
        effective_buildup_types[group] = passive_type_groups[group].reduce((effective_passive, super_passive) =>
            weapon[super_passive] > 0 ? super_passive : effective_passive
        )
    );
    return effective_buildup_types;
}

function get_merged_buildup(weapon, effective_buildup_types) {
    var merged_buildup = {};
    Object.keys(passive_type_groups).forEach(group => {
        passive_type_groups[group].forEach(passive => {
            merged_buildup[passive] = effective_buildup_types[group] == passive ?
                passive_type_groups[group]
                    .map(passive => weapon[passive])
                    .reduce(sum) : 
                0;
        })
    });
    return merged_buildup;
}

function get_scaled_and_modified_passive(buildup, passive_group_id, weapon, modified_attributes, modifiers) {
    if(buildup > 0 && (passive_group_id + '_correction_id') in weapon)
        buildup += buildup * weapon.arc_scaling * get_correction(weapon[passive_group_id + '_correction_id'], modified_attributes.arc) / 100;
    return get_modified_value('passive', buildup, passive_group_id, modifiers, weapon, modified_attributes);
}

function get_passive_hit_damage(buildup, passive_id, weapon, target, hit_damage, stamina_cost, modifiers, hit_modifier_value) {
    return heuristics[passive_id](buildup, target, weapon, hit_damage, Math.max(stamina_cost, 8), modifiers, hit_modifier_value);
}

//Should not use min or max or heuristics to keep smooth data.
self['heuristics'] = {};

heuristics['poison'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['poison'] < 0)
        return 0;
    return get_dot_passive_damage(90, 0.0007, 7, buildup, target.health, target.poison, target.resist_correction_id.poison, hit_damage, stamina_cost);
}

heuristics['deadly_poison'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['poison'] < 0)
        return 0;
    return get_dot_passive_damage(30, 0.0014, 14, buildup, target.health, target.poison, target.resist_correction_id.poison, hit_damage, stamina_cost);
}

heuristics['super_deadly_poison'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['poison'] < 0)
        return 0;
    return get_dot_passive_damage(90, 0.0014, 14, buildup, target.health, target.poison, target.resist_correction_id.poison, hit_damage, stamina_cost);
}

heuristics['rot'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['rot'] < 0)
        return 0;
    return get_dot_passive_damage(90, 0.0018, 15, buildup, target.health, target.rot, target.resist_correction_id.rot, hit_damage, stamina_cost);
}

heuristics['bleed'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['bleed'] < 0)
        return 0;
    return get_instant_passive_damage((target.health * 0.15 + 100) * target.proc_multiplier.bleed, buildup, target.health, target.bleed, target.resist_correction_id.bleed, hit_damage, stamina_cost);
}

heuristics['deadly_bleed'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['bleed'] < 0)
        return 0;
    return get_instant_passive_damage((target.health * 0.15 + 200) * target.proc_multiplier.bleed, buildup, target.health, target.bleed, target.resist_correction_id.bleed, hit_damage, stamina_cost);
}

heuristics['sleep'] = function(buildup, target, weapon, hit_damage, stamina_cost, modifiers, old_hit_modifier_value) {
    if(target['sleep'] < 0)
        return 0;
    var backstab_modifier = get_hit_modifier({moveset_name: 'backstab'}, modifiers).all * weapon.backstab / old_hit_modifier_value;
    return get_instant_passive_damage(hit_damage * backstab_modifier, buildup, target.health, target.sleep, target.resist_correction_id.sleep, hit_damage, stamina_cost);
}

heuristics['madness'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['madness'] < 0)
        return 0;
    return get_instant_passive_damage(target.health * 0.15 + 100, buildup, target.health, target.madness, target.resist_correction_id.madness, hit_damage, stamina_cost);
}

heuristics['death'] = function(buildup, target, weapon, hit_damage, stamina_cost) {
    if(target['death'] < 0)
        return 0;
    return get_instant_passive_damage(target.health, buildup, target.health, target.death, target.resist_correction_id.death, hit_damage, stamina_cost);
}

const base_seconds_per_hit = 1;
const buildup_decay_rate = 5;
const proc_cycles = 2;

heuristics['frost'] = function(buildup, target, weapon, damage_per_hit, stamina_cost) {
    if(target['frost'] < 0 || buildup <= 0)
        return 0;
    
    var health = target.health;
    var resistance = target.frost;
    var resist_correction_id = target.resist_correction_id.frost;
    var proc_multiplier = target.proc_multiplier.frost;
    
    const seconds_per_hit = Math.min(14, base_seconds_per_hit * stamina_cost);
    const buildup_decay = buildup_decay_rate * seconds_per_hit;
    const proc_damage = Math.floor((health * 0.1 + 30) * proc_multiplier);
    const frostbite_damage_bonus = 0.2;
    const frostbite_duration = 30;
    
    var total_proc_damage = proc_damage * proc_cycles;
    var total_hits = get_hits_to_proc(buildup, get_buildup_resistance(resistance, resist_correction_id, 0), buildup_decay);
    
    //proc_cycles + 1 buildup cycles for wasted hits
    for(var i = 1; i <= proc_cycles; i++) {
        total_hits += get_hits_to_proc(buildup, get_buildup_resistance(resistance, resist_correction_id, 0), buildup_decay);
        /*var frostbite_bonus_duration = Math.min(frostbite_duration, hits_to_proc * seconds_per_hit); smoother and more performant to always assume no longer than the frostbite duration.*/
        total_proc_damage += frostbite_duration * damage_per_hit / seconds_per_hit * frostbite_damage_bonus;
    }
    
    return total_proc_damage / total_hits;
}

function get_dot_passive_damage(effect_duration, tic_health_bonus, tic_base, buildup, health, resistance, resist_correction_id, damage_per_hit, stamina_cost) {
    if(buildup <= 0)
        return 0;
    
    const seconds_per_hit = Math.min(14, base_seconds_per_hit * stamina_cost);
    const buildup_decay = buildup_decay_rate * seconds_per_hit;
    const proc_damage_per_second = Math.floor(health * tic_health_bonus + tic_base);
    
    var total_proc_damage = proc_damage_per_second * effect_duration * proc_cycles;
    var total_hits = effect_duration / seconds_per_hit * proc_cycles;
    
    //proc_cycles + 1 buildup cycles for wasted hits
    for(var i = 0; i <= proc_cycles; i++)
        total_hits += get_hits_to_proc(buildup, get_buildup_resistance(resistance, resist_correction_id, i), buildup_decay);
    
    return total_proc_damage / total_hits;
}

function get_instant_passive_damage(proc_damage, buildup, health, resistance, resist_correction_id, damage_per_hit, stamina_cost) {
    if(buildup <= 0)
        return 0;
    
    const seconds_per_hit = Math.min(14, base_seconds_per_hit * stamina_cost);
    const buildup_decay = buildup_decay_rate * seconds_per_hit;
    
    var total_proc_damage = Math.floor(proc_damage) * proc_cycles;
    var total_hits = 0;
    
    //proc_cycles + 1 buildup cycles for wasted hits
    for(var i = 0; i <= proc_cycles; i++)
        total_hits += get_hits_to_proc(buildup, get_buildup_resistance(resistance, resist_correction_id, i), buildup_decay);
    
    return total_proc_damage / total_hits;
}

function get_buildup_resistance(resistance, resist_correction_id, cycle_number) {
    return resistance + resist_corrections[resist_correction_id][Math.min(5 ,cycle_number)];
}

function get_hits_to_proc(buildup, resistance, buildup_decay) {
    var post_initial_resistance = resistance - buildup;
    if(post_initial_resistance <= 0)
        return 1;
    var post_initial_buildup_rate =  buildup - buildup_decay;
    if(post_initial_buildup_rate <= 1)
        return resistance;
    return post_initial_resistance / post_initial_buildup_rate + 1;
}