self['get_modified_value'] = function(category, unmodified, subcategory, modifiers, weapon, attributes) {
    var modified = unmodified;
    var specific_modifiers_for_category = modifiers[category][subcategory];
    var all_modifiers_for_category = modifiers[category].all;
    
    //only applies to Blue Dancer Charm for now, so skipping some checks
    for(var modifier of specific_modifiers_for_category.mapped)
        if(modifier.correction_id == 'bdc')
            modified *= get_correction(modifier.correction_id, modifier.value + weapon.weight);
    
    //multiplicative modifiers
    for(var modifier of specific_modifiers_for_category.percent.concat(all_modifiers_for_category.percent))
        if(attributes_meets_requirements(attributes, modifier.requirements) && (!modifier.enchantment || (weapon !== undefined && weapon.buffable)))
            modified *= 1 + modifier.value / 100;
        
    for(var modifier of specific_modifiers_for_category.inverse.concat(all_modifiers_for_category.inverse))
        if(attributes_meets_requirements(attributes, modifier.requirements) && (!modifier.enchantment || (weapon !== undefined && weapon.buffable)))
            modified = 100 - (100 - modified) * (100 - modifier.value) / 100;
    
    //simple additive modifiers
    for(var modifier of specific_modifiers_for_category.add.concat(all_modifiers_for_category.add))
        if(attributes_meets_requirements(attributes, modifier.requirements) && (!modifier.enchantment || (weapon !== undefined && weapon.buffable)))
            modified += modifier.value;
    
    //attribute scaling, also considered a type of additive modifiers
    if(attributes !== undefined) {
        for(var attribute of attack_sources) {
            for(var modifier of specific_modifiers_for_category[attribute].concat(all_modifiers_for_category[attribute])) {
                if(attributes_meets_requirements(attributes, modifier.requirements)) {
                    if(modifier.enchantment) {
                        if(weapon !== undefined && weapon.buffable)
                            modified += modifier.value * modifier.scaling * get_correction(modifier.correction_id, attributes[attribute]) / 100;
                    }
                    else {
                        modified += modifier.value + modifier.value * weapon[attribute + '_scaling'] * get_correction(modifier.correction_id, attributes[attribute]) / 100;
                    }
                }
            }
        }
    }
    
    return modified;
}

self['parse_effect_into_modifiers'] = function(modifiers, effect, catalyst, reinforcement, args, armor_weight) {
    var category = effect[0];
    var subcategory = effect[1];
    var incrementation = effect[2];
    var modifier = effect[3];
    if(['inc', 'sor'].includes(incrementation)) {
        
        var unsatisfied_modifier = Object.assign({}, modifier);
        unsatisfied_modifier.value *= 0.6;
        modifiers[category][subcategory].add.push(unsatisfied_modifier);
        
        var satisfied_requirements = get_merged_catalyst_requirements(modifier, catalyst);
        var satisfied_modifier = Object.assign({}, modifier);
        satisfied_modifier.value *= 0.4;
        satisfied_modifier.requirements = satisfied_requirements;
        modifiers[category][subcategory]['add'].push(satisfied_modifier);
        
        var reinforcement_entry = reinforce_values[catalyst.reinforce_id + reinforcement];
        for(var attribute of args.attack_sources) {
            if(catalyst.element_scaling['physical_' + attribute] && catalyst[attribute + '_scaling']) {
                var scaling_modifier = Object.assign({}, modifier);
                scaling_modifier.scaling = catalyst[attribute + '_scaling'] * reinforcement_entry[attribute];
                scaling_modifier.correction_id = catalyst.physical_correction_id;
                scaling_modifier.requirements = satisfied_requirements;
                modifiers[category][subcategory][attribute].push(scaling_modifier);
            }
        }
    }
    else {
        if(modifier.correction_id == 'bdc')
            modifier.value = armor_weight;
        modifiers[category][subcategory][incrementation].push(modifier);
    }
}

function get_merged_catalyst_requirements(modifier, catalyst) {
    var requirements = {};
    for(var attribute of attack_sources) {
        var attribute_requirement = modifier.requirements[attribute] ? modifier.requirements[attribute] : 0;
        var catalyst_requirement = catalyst.requirements[attribute];
        
        var merged_attribute_requirement = Math.max(attribute_requirement, catalyst_requirement);
        if(merged_attribute_requirement > 0)
            requirements[attribute] = merged_attribute_requirement;
    }
    return requirements;
}

function get_modified_attributes(attributes, modifiers, options, weapon) {
    var modified_attributes = {};
    for(var attribute in attributes) {
        modified_attributes[attribute] = get_modified_value('attribute', attributes[attribute], attribute, modifiers, weapon, attributes);
        modified_attributes[attribute] = Math.max(1, Math.min(99, modified_attributes[attribute]));
        if(options.is_two_handing && attribute == 'str')
            modified_attributes[attribute] *= 1.5;
    }
    
    return modified_attributes;
}

function get_hit_modifier(moveset, modifiers) {
    var hit_modifier = {all:1,last:1};
    var moveset_name = moveset.moveset_name;
    
    if(moveset_name == 'backstab' || moveset_name == 'riposte')
        for(var modifier of modifiers.movement.critical.percent)
            hit_modifier.all *= 1 + modifier.value / 100;

    if(moveset_name.includes('guardcounter'))
        for(var modifier of modifiers.movement.guardcounter.percent)
            hit_modifier.all *= 1 + modifier.value / 100;

    if(moveset_name.includes('_charged') || moveset_name.includes('charging'))
        for(var modifier of modifiers.movement.charged.percent)
            hit_modifier.all *= 1 + modifier.value / 100;

    if(moveset_name == 'shieldpoke')
        for(var modifier of modifiers.movement.shieldpoke.percent)
            hit_modifier.all *= 1 + modifier.value / 100;

    if(moveset_name.includes('mounted'))
        for(var modifier of modifiers.movement.mounted.percent)
            hit_modifier.all *= 1 + modifier.value / 100;

    if(moveset_name.includes('jumping'))
        for(var modifier of modifiers.movement.jumping.percent)
            hit_modifier.all *= 1 + modifier.value / 100;

    if(['normal_light_1h', 'normal_light_2h', 'normal_offhand', 'normal_paired', 'mounted_light', 'normal'].includes(moveset_name))
        for(var modifier of modifiers.movement.last.percent)
            hit_modifier.last *= 1 + modifier.value / 100;
    
    return hit_modifier;
}

function get_ranged_modifier(modifiers) {
    var ranged_modifier = {all:0, headshot:0};
    
    for(var modifier of modifiers.ranged.all.percent)
        ranged_modifier.all += modifier.value / 100;
    for(var modifier of modifiers.ranged.headshot.percent)
        ranged_modifier.headshot += modifier.value / 100;
    
    return ranged_modifier;
}

function push_weapon_modifiers(modifiers, weapon) {
    for(var effect of weapon.effects)
        modifiers[effect[0]][effect[1]][effect[2]].push(effect[3]);
    
    return modifiers;
}

function pop_weapon_modifiers(modifiers, weapon) {
    for(var effect of weapon.effects)
        modifiers[effect[0]][effect[1]][effect[2]].pop();
}

function get_modified_requirements(modifiers, options) {
    var requirements = [];
    for(var category in modifiers) {
        for(var subcategory in modifiers[category]) {
            for(var incrementor in modifiers[category][subcategory]) {
                for(var modifier of modifiers[category][subcategory][incrementor]) {
                    if(modifier.requirements) {
                        var new_requirement = {};
                        var value = modifier.requirements.str;
                        if(value) {
                            if(options.is_two_handing)
                                value = Math.ceil(value / 1.5);
                            new_requirement.str = Math.max(value - get_requirement_offset('str', modifiers), 0);
                        }
                        for(var attribute of attack_sources.slice(1)) {
                            value = modifier.requirements[attribute];
                            if(value) {
                                value = Math.max(value - get_requirement_offset(attribute, modifiers), 0);
                                new_requirement[attribute] = value;
                            }
                        }
                        new_requirement.enchantment = modifier.enchantment;
                        requirements.push(new_requirement);
                    }
                }
            }
        }
    }
    
    requirements = requirements.filter((req, index, array) => index == array.findIndex(other_req => attack_sources.every(attribute => req[attribute] == other_req[attribute]) && req.enchantment == other_req.enchantment));
    
    return requirements;
}

function get_requirement_offset(req_attribute, modifiers) {
    var modified = 0;
    var specific_modifiers_for_category = modifiers.attribute[req_attribute];
    var all_modifiers_for_category = modifiers.attribute.all;
    
    //multiplicative modifiers
    for(var modifier of specific_modifiers_for_category.percent.concat(all_modifiers_for_category.percent))
        modified *= 1 + modifier.value / 100;
    
    //simple additive modifiers
    for(var modifier of specific_modifiers_for_category.add.concat(all_modifiers_for_category.add))
        modified += modifier.value;
    
    return modified;
}

function get_modifier_scaling_set(modifiers) {
    var list = new Set();
    for(var category in modifiers)
        for(var subcategory in modifiers[category])
            for(var attribute of attack_sources)
                for(var modifier of modifiers[category][subcategory][attribute])
                    if(modifier.enchantment)
                        list.add(attribute);
    return list;
}