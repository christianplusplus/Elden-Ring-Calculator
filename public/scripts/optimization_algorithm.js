self['optimize'] = function(minimum_attributes, optimize_free_points, target_level, classes, weapons, enemy, moveset, modifiers, options) {
    progress_count = 0;
    progress_total = weapons.length * classes.length;
    
    var hit_modifier = get_hit_modifier(moveset, modifiers);
    var objective = get_damage_objective(enemy, moveset, modifiers, options, hit_modifier);
    var result_objective = get_result_objective(enemy, moveset, modifiers, options, hit_modifier);
    var modified_requirements = get_modified_requirements(modifiers, options);
    var modifier_scaling_set = get_modifier_scaling_set(modifiers);
    
    try {
        var result = optimize_class_weapon_attributes(objective, minimum_attributes, optimize_free_points, target_level, classes, weapons, modifiers, options, modified_requirements, modifier_scaling_set);
        if(optimize_free_points)
            result.attributes = allocate_extra_points(result.attributes, minimum_attributes, target_level);
        result = beautify_result(result, objective, result_objective, enemy, modifiers, options);
        return result;
    } catch(e) {
        console.log(e);
        if(e.error)
            return e;
        else
            return {error: 'No Results'};
    }
}

function optimize_class_weapon_attributes(objective, minimum_attributes, optimize_free_points, target_level, classes, weapons, modifiers, options, modified_requirements, modifier_scaling_set) {
    var best_result = {value:0};
    var highest_level = 0;
    var good_fit_classes = [];
    var bad_fit_classes = [];
    var candidate_classes = [];
    var minimum_class_attributes_index = {};
    var free_points_index = {};
    var found_weapon = false;
    var free_points = optimize_free_points ? target_level + 79 : sources.reduce((acc, attribute) => acc + minimum_attributes[attribute], 0);
    
    for(var clazz of classes) {
        var minimum_class_attributes = {};
        var class_free_points = free_points;
        var this_class_is_good_fit = true;
        
        for(var attribute of sources) {
            var minimum_attribute = minimum_attributes[attribute];
            if(optimize_free_points)
                minimum_attribute -= get_requirement_offset(attribute, modifiers);
            if(minimum_attribute < clazz[attribute]) {
                this_class_is_good_fit = false;
                minimum_attribute = clazz[attribute];
            }
            
            class_free_points -= minimum_attribute;
            minimum_class_attributes[attribute] = minimum_attribute;
        }
        
        if(class_free_points >= 0) {
            minimum_class_attributes_index[clazz.name] = minimum_class_attributes;
            free_points_index[clazz.name] = class_free_points;
            
            if(this_class_is_good_fit)
                good_fit_classes.push(clazz);
            else
                bad_fit_classes.push(clazz);
        }
    }
    
    if(good_fit_classes.length) {
        candidate_classes.push(good_fit_classes.sort((a, b) => b.lvl - a.lvl)[0]);
        progress(weapons.length * (classes.length - 1));
    }
    else if(bad_fit_classes.length) {
        candidate_classes = bad_fit_classes;
        progress(weapons.length * (classes.length - bad_fit_classes.length));
    }
    else {
        throw {error: 'Class cannot satisfy attribute selection.'};
    }
    
    for(clazz of candidate_classes) {
        var result;
        try {
            result = optimize_weapon_attributes(objective, minimum_class_attributes_index[clazz.name], free_points_index[clazz.name], weapons, modifiers, options, modified_requirements, modifier_scaling_set);
        }
        catch(e) {
            if(e.error && e.error == 'Couldn\'t find wieldable weapon.')
                continue;
            else
                throw e;
        }
        found_weapon = true;
        if((result.value > best_result.value) || (result.value == best_result.value && clazz.lvl > highest_level)) {
            Object.assign(best_result, result, {class: clazz});
            highest_level = clazz.lvl;
        }
    }
    
    if(!found_weapon)
        throw {error: 'Couldn\'t find wieldable weapon.'};
    
    return best_result;
}

function optimize_weapon_attributes(objective, minimum_attributes, free_points, weapons, modifiers, options, modified_requirements, modifier_scaling_set) {
    var best_result = {value: 0}
    var found_weapon = false;
    
    for(var weapon of weapons) {
        var result;
        var minimum_weapon_attributes = get_minimum_weapon_attributes(weapon, minimum_attributes, free_points, modifiers, options);
        var scaling_list = new Set();
        for(var attribute of attack_sources)
            if(weapon[attribute + '_scaling'] || (weapon.buffable && modifier_scaling_set.has(attribute)))
                scaling_list.add(attribute);
        scaling_list = [...scaling_list];
        
        //minimum_weapon_attributes is null if get_minimum_weapon_attributes is unsuccessful.
        try {
            if(minimum_weapon_attributes && !options.brute_force) {
                var free_points_after_weapon_requirements = free_points + get_attack_attribute_sum(minimum_attributes) - get_attack_attribute_sum(minimum_weapon_attributes);
                result = optimize_attributes_with_ascent_solver(objective, minimum_weapon_attributes, free_points_after_weapon_requirements, weapon, modified_requirements, scaling_list);
            }
            else if(options.brute_force || !options.must_have_required_attributes) {
                result = optimize_attributes_with_brute_solver(objective, minimum_attributes, free_points, weapon, modified_requirements, scaling_list);
            }
        }
        catch(e) {
            if(e.error && e.error == 'Couldn\'t find stat distribution.')
                continue;
            else
                throw e;
        }
        finally {
            progress();
        }
        
        if(result) {
            found_weapon = true;
            if(result.value > best_result.value) {
                Object.assign(best_result, result, {weapon: weapon});
                non_attack_sources.forEach(attribute => best_result.attributes[attribute] = minimum_attributes[attribute]);
            }
        }
    }
    
    if(!found_weapon)
        throw {error: 'Couldn\'t find wieldable weapon.'};
    
    return best_result;
}

function optimize_attributes_with_ascent_solver(objective, minimum_weapon_attributes, free_points, weapon, modified_requirements, scaling_list) {
    var best_result = {value: 0};
    var found_result = false;
    
    var attribute_clamps = get_attribute_clamps(minimum_weapon_attributes, modified_requirements, weapon);
    for(var attribute_clamp of attribute_clamps) {
        var free_points_after_clamping = free_points - get_attack_attribute_sum(attribute_clamp.floor) + get_attack_attribute_sum(minimum_weapon_attributes);
        if(free_points_after_clamping < 0)
            continue;
        var result = optimize_clamped_attributes_with_ascent_solver(objective, attribute_clamp, free_points_after_clamping, weapon, scaling_list);
        if(result.value > best_result.value) {
            found_result = true;
            Object.assign(best_result, result);
        }
    }
    
    if(!found_result)
        throw {error: 'Couldn\'t find stat distribution.'}
    
    return best_result;
}

function optimize_clamped_attributes_with_ascent_solver(objective, attribute_clamp, free_points, weapon, scaling_list) {
    var initial_attribute_distribution = get_initial_attribute_distribution(attribute_clamp, free_points);
    
    var constraints = get_attr_constraints(attribute_clamp);
    
    var solution_state = ascent_solver(objective, {weapon: weapon, attributes: initial_attribute_distribution}, get_state_generator(scaling_list), constraints);
    
    var result = {value: solution_state[0], attributes: solution_state[1].attributes};
    return result;
}

function optimize_attributes_with_brute_solver(objective, minimum_attributes, free_points, weapon, modified_requirements, scaling_list) {
    var best_result = {value: 0};
    var found_result = false;
    
    var attribute_clamps = get_attribute_clamps(minimum_attributes, modified_requirements.concat(weapon.requirements), weapon);
    for(var attribute_clamp of attribute_clamps) {
        var free_points_after_clamping = free_points - get_attack_attribute_sum(attribute_clamp.floor) + get_attack_attribute_sum(minimum_attributes);
        if(free_points_after_clamping < 0)
            continue;
        var result = optimize_clamped_attributes_with_bute_solver(objective, attribute_clamp, free_points_after_clamping, weapon, scaling_list);
        if(result.value > best_result.value) {
            found_result = true;
            Object.assign(best_result, result);
        }
    }
    
    if(!found_result)
        throw {error: 'Couldn\'t find stat distribution.'}
    
    return best_result;
}

function optimize_clamped_attributes_with_bute_solver(objective, attribute_clamp, free_points, weapon, scaling_list) {
    var attribute_combinations = get_attack_attribute_combinations(attribute_clamp, free_points, scaling_list);
    
    var weapon_attribute_states = get_weapon_attribute_states(weapon, attribute_combinations);
    
    var solution_state = brute_solver(objective, weapon_attribute_states);
    
    var result = {value: solution_state[0], attributes: solution_state[1].attributes}
    return result;
}


function get_attribute_clamps(minimum_weapon_attributes, modified_requirements, weapon) {
    
    //filter out enchantments, then create copies without any non-attribute keys
    var minimized_requirements = [];
    for(var modifier_requirement of modified_requirements) {
        if(modifier_requirement.enchantment && !weapon.buffable)
            continue;
        var minimized_requirement = {};
        for(var key in modifier_requirement)
            if(key != 'enchantment')
                minimized_requirement[key] = modifier_requirement[key];
        minimized_requirements.push(minimized_requirement);
    }
    
    //create base floor and ceiling
    var global_floor = {};
    var global_ceiling = {};
    for(var attribute of attack_sources) {
        global_floor[attribute] = minimum_weapon_attributes[attribute];
        global_ceiling[attribute] = 100;
    }
    
    var attribute_clamps = [];
    for(var requirement_combination of binary_powerset(minimized_requirements)) {
        var floor = requirement_combination.included.reduce(
            (acc, curr) => {
                for(var attribute in curr)
                    acc[attribute] = Math.max(acc[attribute], curr[attribute]);
                return acc;
            },
            Object.assign({}, global_floor)
        );
        
        //build key combination ceilings
        var ceilings = [];
        for(var ceiling of key_combinations(requirement_combination.excluded)) {
            //ceilings are now lists of object entries
            //reduce back into objects
            ceiling = ceiling.reduce(
                (ceiling_object, [key, value]) => {
                    ceiling_object[key] = Math.min(ceiling_object[key], value);
                    return ceiling_object;
                },
                Object.assign({}, global_ceiling)
            );
            
            ceilings.push(ceiling);
        }
        
        //remove invalid clamps, remove duplicates ceilings, add default ceiling if nothing left
        if(ceilings.length) {
            ceilings = ceilings.filter(ceil => attack_sources.every(attribute => ceil[attribute] > floor[attribute]));
            ceilings = ceilings.filter((ceil, index, array) => index == array.findIndex(other_ceil => attack_sources.every(attribute => ceil[attribute] == other_ceil[attribute])));
        }
        else
            ceilings.push(global_ceiling);
        
        for(var ceiling of ceilings)
            attribute_clamps.push({floor: floor, ceiling: ceiling});
    }
    
    //remove duplicates clamps or add default clamps if nothing left
    if(attribute_clamps.length)
        attribute_clamps = attribute_clamps.filter((clamp, index, array) => index == array.findIndex(other_clamp => attack_sources.every(attribute => clamp.floor[attribute] == other_clamp.floor[attribute] && clamp.ceiling[attribute] == other_clamp.ceiling[attribute])));
    else
        attribute_clamps.push({floor: global_floor, ceiling: global_ceiling});
    
    return attribute_clamps;
}

function get_minimum_weapon_attributes(weapon, minimum_attributes, free_attributes, modifiers, options) {
    var locked_attribute_distribution = {};
    var attributes_needed = Math.max((options.is_two_handing ? Math.ceil(weapon.requirements.str / 1.5) : weapon.requirements.str) - minimum_attributes.str - get_requirement_offset('str', modifiers), 0);
    locked_attribute_distribution.str = minimum_attributes.str + attributes_needed;
    free_attributes -= attributes_needed;
    attack_sources.slice(1).forEach(attack_source => {
        attributes_needed = Math.max(weapon.requirements[attack_source] - minimum_attributes[attack_source] - get_requirement_offset(attack_source, modifiers), 0);
        locked_attribute_distribution[attack_source] = minimum_attributes[attack_source] + attributes_needed;
        free_attributes -= attributes_needed;
    });
    non_attack_sources.forEach(attribute => locked_attribute_distribution[attribute] = minimum_attributes[attribute]);
    return free_attributes >= 0 ? locked_attribute_distribution : null;
}

function get_initial_attribute_distribution(attribute_clamp, free_attributes) {
    var initial_attribute_distribution = {};
    attack_sources.forEach(attack_source => {
        var attributes_needed = Math.min(attribute_clamp.ceiling[attack_source] - attribute_clamp.floor[attack_source] - 1, free_attributes);
        initial_attribute_distribution[attack_source] = attribute_clamp.floor[attack_source] + attributes_needed;
        free_attributes -= attributes_needed;
    });
    return initial_attribute_distribution;
}

function get_weapon_attribute_states(weapon, attribute_combinations) {
    var weapon_attribute_states = {weapon: weapon, attributes: attribute_combinations};
    weapon_attribute_states[Symbol.iterator] = function(){
        var old_iterator = weapon_attribute_states.attributes[Symbol.iterator]();
        return { next: function() {
            var next_object = old_iterator.next();
            return {
                done: next_object.done,
                value: {weapon: weapon, attributes: next_object.value}
            };
        }};
    };
    return weapon_attribute_states;
}

function get_damage_objective(enemy, moveset, modifiers, options, hit_modifier) {
    return function(state) {
        return get_aggregate_attack_damage(state.weapon, state.attributes, modifiers, options, enemy, aggregators[moveset.moveset_aggregate], aggregators[moveset.hit_aggregate], hit_modifier);
    };
}

function get_result_objective(enemy, moveset, modifiers, options, hit_modifier) {
    return function(state) {
        return get_aggregate_attack_damage(state.weapon, state.attributes, modifiers, options, enemy, floor_aggregators[moveset.moveset_aggregate], floor_aggregators[moveset.hit_aggregate], hit_modifier);
    };
}

function get_state_generator(scaling_list) {
    return function state_generator(state) {
        var deltas = [20, 5, 1];
        var new_states = [];
        for(var source of attack_sources) {
            for(var otherSource of scaling_list) {
                if(otherSource != source) {
                    for(var delta of deltas) {
                        var attributes = {};
                        for(var source_name of attack_sources)
                            attributes[source_name] = state.attributes[source_name];
                        attributes[source] -= delta;
                        attributes[otherSource] += delta;
                        new_states.push({weapon: state.weapon, attributes: attributes});
                    }
                }
            }
        }
        return new_states;
    }
}



function get_attr_constraints(attribute_clamp) {
    var constraints = [];
    for(let source of attack_sources) {
        constraints.push((s => x => x.attributes[s] >= attribute_clamp.floor[s])(source));
        constraints.push((s => x => x.attributes[s] < attribute_clamp.ceiling[s])(source));
    }
    return constraints;
}

function ascent_solver(objective, initial_state, state_generator, constraints) {
    var highest_value = objective(initial_state);
    var next_state = initial_state;
    do {
        var state = next_state;
        next_state = null;
        for(var candidate_state of state_generator(state)) {
            if(constraints.every(constraint => constraint(candidate_state))) {
                var value = objective(candidate_state);
                if(value > highest_value) {
                    highest_value = value;
                    next_state = candidate_state;
                }
            }
        }
    }while(next_state);
    return [highest_value, state];
}

function brute_solver(objective, state_space) {
    var optimal_state = state_space[Symbol.iterator]().next().value;
    var highest_value = objective(optimal_state);
    for(var state of state_space) {
        var value = objective(state);
        if(value > highest_value) {
            highest_value = value;
            optimal_state = state;
        }
    }
    return [highest_value, optimal_state];
}

var minimum_attributes_cache = null;
var maximum_attributes_cache = null;
var free_points_cache = null;
var scaling_list_cache = null;
var attack_attribute_combinations_cache;
function get_attack_attribute_combinations(attribute_clamp, free_attributes, scaling_list) {
    if(
        minimum_attributes_cache !== null
        && attack_sources.every(source => minimum_attributes_cache[source] == attribute_clamp.floor[source])
        && attack_sources.every(source => maximum_attributes_cache[source] == attribute_clamp.ceiling[source])
        && free_points_cache == free_attributes
        && scaling_list.length == scaling_list_cache.length
        && [...scaling_list.entries()].every(([key, value]) => scaling_list_cache[key] == value)
    ) return attack_attribute_combinations_cache;
    
    var attribute_ranges = {};
    for(var source of attack_sources) {
        if(scaling_list.includes(source))
            attribute_ranges[source] = [...inclusiveRange(attribute_clamp.floor[source], Math.min(attribute_clamp.ceiling[source] - 1, attribute_clamp.floor[source] + free_attributes))];
        else
            attribute_ranges[source] = [attribute_clamp.floor[source]];
    }
    
    var max_permutation = {};
    var free_points_at_max = free_attributes;
    for(var source of attack_sources) {
        max_permutation[source] = attribute_ranges[source][attribute_ranges[source].length - 1];
        free_points_at_max += attribute_clamp.floor[source] - max_permutation[source];
    }
    
    var attribute_combinations = [];
    if(free_points_at_max >= 0)
        attribute_combinations.push(max_permutation);
    else
        for(var permutation of permutateKeys(attribute_ranges))
            if(get_attack_attribute_sum(permutation) == free_attributes + get_attack_attribute_sum(attribute_clamp.floor))
                attribute_combinations.push(permutation);
    
    minimum_attributes_cache = Object.assign({}, attribute_clamp.floor);
    maximum_attributes_cache = Object.assign({}, attribute_clamp.ceiling);
    free_points_cache = free_attributes;
    scaling_list_cache = scaling_list.slice();
    attack_attribute_combinations_cache = attribute_combinations;
    return attribute_combinations;
}

function allocate_extra_points(attributes, minimum_attributes, target_level) {
    var new_attributes = {};
    var free_points = target_level + 79;
    for(var attribute of sources) {
        if(attribute in attributes) {
            new_attributes[attribute] = attributes[attribute];
            free_points -= attributes[attribute];
        }   
        else {
            new_attributes[attribute] = minimum_attributes[attribute]
            free_points -= minimum_attributes[attribute];
        }
    }
    
    for(var attribute of attack_sources.concat(non_attack_sources)) {
        var new_value = Math.min(99, new_attributes[attribute] + free_points);
        free_points += new_attributes[attribute] - new_value;
        new_attributes[attribute] = new_value;
    }
    
    return new_attributes;
}











