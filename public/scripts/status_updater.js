self['status_update'] = function(){console.log('No status_update function defined.')};

var progress_count = 0;
var progress_total = 100;
function progress(units = 1) {
    progress_count += units;
    self['status_update'](progress_count / progress_total);
}