function beautify_result(result, objective, result_objective, target, modifiers, options) {
    var modified_attributes = get_modified_attributes(result.attributes, modifiers, options);
    
    var base_attack_power = attack_types.map(attack_type => ({[attack_type]:
            get_base_attack_power(result.weapon, attack_type)
        })).reduce((a,b) => Object.assign(a,b),{});
        
    var bonus_attack_power = attack_types.map(attack_type => ({[attack_type]:
            get_bonus_attack_power(result.weapon, attack_type, modified_attributes, modifiers, options)
        })).reduce((a,b) => Object.assign(a,b),{});
        
    var total_attack_power = attack_types.map(attack_type => ({[attack_type]:
            get_modified_value('attack', base_attack_power[attack_type] + bonus_attack_power[attack_type], attack_type, modifiers, result.weapon, modified_attributes)
        })).reduce((a,b) => Object.assign(a,b),{});
        
    var modded_attack_power = attack_types.map(attack_type => ({[attack_type]:
            total_attack_power[attack_type] - bonus_attack_power[attack_type] - base_attack_power[attack_type]
        })).reduce((a,b) => Object.assign(a,b),{});
    
    var result_object = {
        class: result.class,
        attributes: result.attributes,
        name: result.weapon.name,
        base_weapon_name : result.weapon.base_weapon_name,
        affinity: result.weapon.affinity,
        weight: result.weapon.weight,
        weapon_type: result.weapon.weapon_type,
        upgrade_level: result.weapon.upgrade_level,
        buffable: result.weapon.buffable,
        required_str: result.weapon.requirements.str,
        required_dex: result.weapon.requirements.dex,
        required_int: result.weapon.requirements.int,
        required_fai: result.weapon.requirements.fai,
        required_arc: result.weapon.requirements.arc,
        str_scaling_grade: get_scaling_grade(result.weapon.str_scaling),
        dex_scaling_grade: get_scaling_grade(result.weapon.dex_scaling),
        int_scaling_grade: get_scaling_grade(result.weapon.int_scaling),
        fai_scaling_grade: get_scaling_grade(result.weapon.fai_scaling),
        arc_scaling_grade: get_scaling_grade(result.weapon.arc_scaling),
        physical_damage_types: result.weapon.physical_damage_types,
        
        base_attack_power: Object.entries(base_attack_power).map(([attack_type, ap]) => ({[attack_type]:
                Math.floor(ap)
            })).reduce((a,b) => Object.assign(a,b),{}),
            
        bonus_attack_power: Object.entries(bonus_attack_power).map(([attack_type, ap]) => ({[attack_type]:
                Math.floor(ap)
            })).reduce((a,b) => Object.assign(a,b),{}),
            
        total_attack_power: Object.entries(total_attack_power).map(([attack_type, ap]) => ({[attack_type]:
                Math.floor(ap)
            })).reduce((a,b) => Object.assign(a,b),{}),
            
        modded_attack_power: Object.entries(modded_attack_power).map(([attack_type, ap]) => ({[attack_type]:
                Math.round(ap)
            })).reduce((a,b) => Object.assign(a,b),{}),
        
        modifiers: push_weapon_modifiers(modifiers, result.weapon),
    };
    
    Object.assign(result_object, get_aggregate_buildup(result.weapon, modified_attributes, modifiers));
    passive_types.forEach(passive_id => {
        result_object[passive_id] = Math.floor(result_object[passive_id]);
    });
    
    var passive_contribution_breakdown = get_passive_contribution_breakdown(objective, result, target);
    result_object.passive_contribution = {};
    for(var passive in passive_contribution_breakdown) {
        result_object.passive_contribution[passive] = Math.round(passive_contribution_breakdown[passive] * 10) / 10;
    }
    
    options.passive_damage_heuristics = false;
    
    result_object.damage = Math.round(result_objective(result) * 10) / 10;
    
    return result_object;
}

function get_scaling_grade(scaling) {
    scaling = parseFloat(scaling);
    if(scaling > 1.75)
        return 'S';
    if(scaling >= 1.4)
        return 'A';
    if(scaling >= 0.9)
        return 'B';
    if(scaling >= 0.6)
        return 'C';
    if(scaling >= 0.25)
        return 'D';
    if(scaling > 0)
        return 'E';
    return '-';
}

function get_passive_contribution_breakdown(objective, result, target) {
    var weapon = result.weapon;
    var attributes = result.attributes;
    
    var temp_passive_values = {};
    for(var passive_type in passive_type_groups) {
        temp_passive_values[passive_type] = target[passive_type];
        target[passive_type] = -1;
    }
    
    var baseline = objective(result);
    
    var passive_contribution_breakdown = {};
    for(var passive_type in passive_type_groups) {
        target[passive_type] = temp_passive_values[passive_type];
        passive_contribution_breakdown[passive_type] = objective(result) - baseline;
        target[passive_type] = -1;
    }
    
    for(var passive_type in passive_type_groups) {
        target[passive_type] = temp_passive_values[passive_type];
    }
    
    return passive_contribution_breakdown;
}