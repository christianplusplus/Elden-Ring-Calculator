function get_aggregate_attack_damage(weapon, attributes, modifiers, options, target, moveset_aggregate, hit_aggregate, hit_modifier) {
    push_weapon_modifiers(modifiers, weapon);
    
    var modified_attributes = get_modified_attributes(attributes, modifiers, options, weapon);
    
    var attack_powers = get_attack_powers(weapon, modified_attributes, modifiers, options);
    
    var damage = moveset_aggregate(weapon.moveset, (move, is_last_move) => hit_aggregate(move, (hit, is_last_hit) => get_movement_damage(attack_powers, target, hit[0], hit[1], hit[2], modifiers, hit_modifier, is_last_move, weapon, modified_attributes, options)));
    
    pop_weapon_modifiers(modifiers, weapon);
    
    return damage;
}

function get_attack_powers(weapon, modified_attributes, modifiers, options) {
    var attack_powers = {};
    for(var attack_type of attack_types) {
        var base = get_base_attack_power(weapon, attack_type);
        var bonus = get_bonus_attack_power(weapon, attack_type, modified_attributes, modifiers, options);
        attack_powers[attack_type] = get_modified_value('attack', base + bonus, attack_type, modifiers, weapon, modified_attributes);
    }
    return attack_powers;
}

function get_movement_damage(attack_powers, target, movement_value, swing_type, stamina_cost, modifiers, hit_modifier, is_last_move, weapon, attributes, options) {
    var damage = Object.entries(attack_powers).map(([attack_type, attack_power]) => get_type_damage(attack_type, attack_power, target, movement_value, swing_type, attributes, modifiers, weapon, options)).reduce(sum);
    
    var hit_modifier_value = hit_modifier.all;
    
    if(weapon.ammo != 'N/A')
        damage = get_modified_value('ranged', damage, options.headshot ? 'headshot' : 'noheadshot', modifiers);
    
    else if(is_last_move)
        hit_modifier_value *= hit_modifier.last;
    
    damage *= hit_modifier_value;
    
    damage = get_modified_value('target', damage, target.type, modifiers, weapon, attributes);
    
    if(options.passive_damage_heuristics)
        damage += get_all_passive_hit_damage(weapon, target, attributes, damage, stamina_cost, modifiers, hit_modifier_value);
    
    return damage;
}

function get_type_damage(attack_type, attack_power, target, movement_value, swing_type, attributes, modifiers, weapon, options) {
    if(swing_type == 'magic' && attack_type != 'magic')
        return 0;
    var resistance = attack_type == 'physical' ? target[swing_type] : target[attack_type];
    
    attack_power *= movement_value;
    if(options.custom_moveset && options.use_crit)
        attack_power *= weapon.crit;
    
    var damage = mitigate_damage(attack_power, target.defense) * resistance;
    
    damage = get_modified_value('damage', damage, attack_type, modifiers, weapon, attributes);
    
    return damage;
}

function get_base_attack_power(weapon, attack_type) {
    return weapon[attack_type + '_attack_power'];
}

function get_bonus_attack_power(weapon, attack_type, attributes, modifiers, options) {
    var source_attack_power;
    if(attack_sources.every( source => meets_requirement(weapon, attack_type, attributes, source, modifiers, options))) {
        var source_attack_powers = attack_sources.map( source => get_attack_power_per_source(weapon, attack_type, attributes[source], source, modifiers, options));
        source_attack_power = source_attack_powers.reduce(sum);
    }
    else {
        source_attack_power = parseFloat(weapon[attack_type + '_attack_power']) * -0.4;
    }
    return source_attack_power;
}

function get_attack_power_per_source(weapon, attack_type, attribute, source, modifiers, options) {
    if(!can_scale(weapon, attack_type, source))
        return 0;
    var base_attack_power = get_base_attack_power(weapon, attack_type);
    var bonus_attack_power_scaling = weapon[source +'_scaling'];
    var calculation_id = weapon[attack_type + '_correction_id'];
    var attribute_correction = get_correction(calculation_id, attribute) / 100;
    var bonus_attack_power = base_attack_power * bonus_attack_power_scaling * attribute_correction;
    return bonus_attack_power;
}

function meets_requirement(weapon, attack_type, attributes, source, modifiers, options) {
    return !can_scale(weapon, attack_type, source) || attributes[source] >= weapon.requirements[source];
}

function can_scale(weapon, attack_type, source) {
    return weapon['element_scaling'][attack_type + '_' + source];
}

self['get_correction'] = function(index, input) {
    var values = correction_values[index];
    var x = values.x;
    var y = values.y;
    var e = values.e;
    var i = 0;
    input = Math.min(input, x[4]);
    while(input > x[i+1]) i++;
    
    return e[i] >= 0 ?
        y[i] + (y[i+1] - y[i]) * ((input - x[i]) / (x[i+1] - x[i])) ** e[i] :
        y[i] + (y[i+1] - y[i]) * (1 - (1 - (input - x[i]) / (x[i+1] - x[i])) ** (-e[i]));
}

function mitigate_damage(attack_power, defense) {
    var damage;
    if(defense > attack_power * 8)
        damage = 0.1 * attack_power;
    else if(defense > attack_power)
        damage = attack_power * (19.2 / 49 * (attack_power / defense - 0.125) ** 2 + 0.1);
    else if(defense > attack_power * 0.4)
        damage = attack_power * (-0.4 / 3 * (attack_power / defense - 2.5) ** 2 + 0.7);
    else if(defense > attack_power * 0.125)
        damage = attack_power * (-0.8 / 121 * (attack_power / defense - 8) ** 2 + 0.9);
    else
        damage = attack_power * 0.9;
    return damage;
}