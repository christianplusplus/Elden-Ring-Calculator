//window[window.btoa('signature')]()
self['c2lnbmF0dXJl'] = function(){return window.atob('Q29kZSB3cml0dGVuIGJ5IENocmlzdGlhbiBXZW5kbGFuZHQuIEFsbCByaWdodHMgcmVzZXJ2ZWQu');};

var attack_types = [
    'physical',
    'magic',
    'fire',
    'lightning',
    'holy',
];

var passive_types = [
    'bleed',
    'rot',
    'death',
    'sleep',
    'poison',
    'deadly_poison',
    'super_deadly_poison',
    'deadly_bleed',
    'frost',
    'madness',
];

var passive_type_groups = {
    poison: ['poison', 'deadly_poison', 'super_deadly_poison'],
    bleed: ['bleed', 'deadly_bleed'],
    rot: ['rot'],
    frost: ['frost'],
    madness: ['madness'],
    sleep: ['sleep'],
    death: ['death'],
};

var sources = [
    'vig',
    'min',
    'end',
    'str',
    'dex',
    'int',
    'fai',
    'arc',
]

var attack_sources = [
    'str',
    'dex',
    'int',
    'fai',
    'arc',
];

var non_attack_sources = sources.filter(source => !attack_sources.includes(source));

var aggregators = {
    first: (arr, func) => (arr.length && arr.length > 0) ? func(arr[0], false) : 0,
    last: (arr, func) => (arr.length && arr.length > 0) ? func(arr[arr.length - 1], true) : 0,
    total: (arr, func) => {
        if(!(arr.length && arr.length > 0))
            return 0;
        var sum = 0;
        for(var i = 0; i < arr.length - 1; i++)
            sum += func(arr[i], false);
        sum += func(arr[arr.length - 1], true);
        return sum;
    },
    average: (arr, func) => {
        if(!(arr.length && arr.length > 0))
            return 0;
        var sum = 0;
        for(var i = 0; i < arr.length - 1; i++)
            sum += func(arr[i], false);
        sum += func(arr[arr.length - 1], true);
        return sum / arr.length;
    },
}

var floor_aggregators = {};
for(var key in aggregators) {
    let base_aggregator = aggregators[key];
    floor_aggregators[key] = (arr, func) => base_aggregator(arr,
        (...args) => Math.floor(func(...args))
    )
}