var reinforce_values;
fetch('data/reinforce_values.json')
    .then(response => response.json())
    .then(data => {
        reinforce_values = data;
    });
var correction_values;
fetch('data/correction_values.json')
    .then(response => response.json())
    .then(data => {
        correction_values = data;
    });
var resist_corrections;
fetch('data/resist_corrections.json')
    .then(response => response.json())
    .then(data => {
        resist_corrections = data;
    });

importScripts(
    'scripts/beautifier.js',
    'scripts/buildup.js',
    'scripts/damage_calculator.js',
    'scripts/globals.js',
    'scripts/modifiers.js',
    'scripts/optimization_algorithm.js',
    'scripts/status_updater.js',
    'scripts/util.js',
);

status_update = function(progress) {
    postMessage({header: 'update', progress: progress});
};

onmessage = function(e) {
    var runnable = eval(e.data.runnable);
    var args = JSON.parse(e.data.args);
    var result = runnable(...args);
    postMessage({header: 'result', result: result});
};